"""_____________________________________________________________________

:PROJECT: LARA

*lara_parts apps *

:details: lara_parts app configuration. This provides a genaric 
         django app configuration mechanism.
         For more details see:
         https://docs.djangoproject.com/en/2.0/ref/applications/

:file:    apps.py
:authors: 

:date: (creation)          
:date: (last modification) 

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.1"

from django.apps import AppConfig

class LaraPartsConfig(AppConfig):
    name = 'lara_parts'
    # verbose_name = "enter a verbose name for your app: lara_parts here - this will be used in the admin interface"
    # lara_app_icon = 'lara_parts_icon.svg'  # this will be used to display an icon, e.g. in the main LARA menu.

