"""_____________________________________________________________________

:PROJECT: LARA

*lara_parts admin *

:details: lara_parts admin module admin backend configuration.
         - 

:file:    admin.py
:authors: 

:date: (creation)          
:date: (last modification) 

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

from django.contrib import admin

from .models import ExtraData, PartInstance, PartOrder

admin.site.register(PartInstance)
admin.site.register(PartOrder)

admin.site.register(ExtraData)
