"""_____________________________________________________________________

:PROJECT: LARA

*lara_parts models *

:details: lara_parts database models. 
         -

:file:    models.py
:authors: 

:date: (creation)          
:date: (last modification) 

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.2"

import uuid
from datetime import date, datetime, timedelta

from django.db import models

from lara.models import DataType, MediaType, ItemStatus, Location, Currency, Namespace, Tag
from lara_people.models import Entity
from lara_library.models import LibItem
from lara_material.models import Part

class ExtraData(models.Model):
    """This class can be used to extend the Entity data, by extra information, 
       e.g. special customer numbers, special discount, RFID and advanced Network parameters ... """
    extra_data_id =  models.AutoField(primary_key=True)
    data_type = models.ForeignKey(DataType, related_name="%(app_label)s_%(class)s_datatype_related",
                                  on_delete=models.CASCADE, null=True, blank=True)
    extra_text = models.TextField(blank=True, null=True, help_text="generic text field")  
    extra_bin = models.BinaryField(blank=True, null=True, help_text="generic binary field")
    media_type = models.ForeignKey(MediaType, related_name="%(app_label)s_%(class)s_mediatype_related",
                                  on_delete=models.CASCADE, null=True, blank=True,  help_text="file type of extra data file")
    extra_file = models.FileField(upload_to='lara_parts_store/', blank=True, null=True, help_text="rel. path/filename")
    URL = models.URLField(blank=True, help_text="Universal Resource Locator - URL")
    description = models.TextField(blank=True, null=True, help_text="description of extra data")

    def __str__(self):
        return "{} ({})".format(self.data_type.data_type, self.description) or "" 

    def __repr__(self):
        return "{} ({})".format(self.data_type.data_type, self.description) or ""

    class Meta:
        verbose_name_plural = 'ExtraData'

class PartInstance(models.Model):
    """ Abstract Store class 
    """
    part_instance_id =  models.AutoField(primary_key=True)
    part = models.ForeignKey(Part, related_name="part_part_instances",
                             on_delete=models.CASCADE, null=True, blank=True)
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_namespace_related', 
                      related_query_name="%(app_label)s_%(class)s_namespace",
                      on_delete=models.CASCADE, null=True, blank=True, 
                      help_text="namespace of part")
    
    barcode1D = models.TextField(blank=True, null=True, help_text="1D barcode of location")
    # -> JSON
    barcode2D = models.TextField(blank=True, null=True, help_text="2D barcode of location - binary")
    UUID = models.UUIDField(default=uuid.uuid4, help_text="UUID of the item")
    registration_no = models.TextField(blank=True, help_text="can be used for internal registrations in a labstore") 
    name_DNS = models.TextField(blank=True, help_text="DNS-Domain Name Service name of part or device")
    name_MAC = models.TextField(blank=True, help_text="MAC address of part or device")
    IP_static = models.TextField(blank=True, help_text="static IP address of part or device")
    vendor = models.ForeignKey(Entity, related_name="%(app_label)s_%(class)s_vendor_related", 
                               related_query_name="%(app_label)s_%(class)s_vendor", 
                               on_delete=models.CASCADE, blank=True, null=True, 
                               help_text="vendor of the device")
    vendor_product_no = models.TextField(blank=True, help_text="vendor product number")
    serial_no = models.TextField(blank=True, help_text="part/devices serial number")
    service_no = models.TextField(blank=True, help_text="service number or tag")

    manifacturing_date = models.DateField(default=date(1,1,1), help_text="date of manufaction")
    # price - try also https://github.com/django-money/django-money
    price = models.DecimalField(max_digits=8, decimal_places=2, help_text="price of the part in default currency/EURO")
    purchase_date = models.DateField(default=date(1,1,1), help_text="") 
    end_of_warrenty_date = models.DateField(default="0001-01-01", help_text="")
    location = models.ForeignKey(Location, related_name="location_partinstances",
                                on_delete=models.CASCADE, null=True, blank=True, 
                                help_text="location of container")
    operators = models.ManyToManyField(Entity,related_name="%(app_label)s_%(class)s_operators_related", 
                                  related_query_name="%(app_label)s_%(class)s_operators", 
                                  blank=True,  
                                  help_text="people, who are instructed to operate this part or device")
    service_group = models.ManyToManyField(Entity, related_name="%(app_label)s_%(class)s_service_group_related", 
                                      related_query_name="%(app_label)s_%(class)s_service_group",
                                      blank=True, help_text="people, who service this part/device")
    literature = models.ManyToManyField(LibItem, related_name="%(app_label)s_%(class)s_literature_related", 
                                        related_query_name="%(app_label)s_%(class)s_literature",  blank=True, 
                                        help_text="literature regarding this instance a part")
    hash_SHA256 = models.CharField(max_length=256, blank=True, null=True, 
                                   help_text="SHA256 hash of item for identity/dublicate checking")
    extra_data = models.ManyToManyField(ExtraData, related_name="%(app_label)s_%(class)s_extradata_related", 
                                        related_query_name="%(app_label)s_%(class)s_extradata", blank=True, 
                                        help_text="e.g. specific manual for this part")
                
    def warrentyLeft(self, day_interval = 7):
        """ choose day_interval = 7 for weeks
        """
        now = datetime.now()
        warrenty_end = datetime.combine( self.end_of_warrenty_date, datetime.min.time()) # datetime object is required for substraction
        warrenty_left = warrenty_end - now 
        
        return warrenty_left.days / day_interval 
        
    warrentyLeft.admin_order_field = 'end_of_warrenty_date'
    warrentyLeft.short_description = "Warrenty left [weeks]"
         
    def endOfWarrentySoon(self):
        now = datetime.now()
        warrenty_end = datetime.combine( self.end_of_warrenty_date, datetime.min.time())
        warrenty_left =  warrenty_end - now
        
        return warrenty_left < timedelta(weeks=12)
        
    endOfWarrentySoon.admin_order_field = 'end_of_warrenty_date'
    endOfWarrentySoon.boolean = True
    endOfWarrentySoon.short_description = 'Warrenty ends soon ?'
        
    def inWarrentyPeriode(self):
        today = date.today()

        return( self.end_of_warrenty_date > today )
        
    inWarrentyPeriode.admin_order_field = 'end_of_warrenty_date'
    inWarrentyPeriode.boolean = True
    inWarrentyPeriode.short_description = 'Still Warrenty ?'
    
    def save(self, force_insert=None, using=None):
        """ Here we generate some default values for full_name """
        if not self.full_name:
            self.name = ' '.join((self.name, self.model_no, serial_no))
                       
        if not self.name_slug:
            self.name_slug = '-'.join((self.name, self.model_no, serial_no )).lower()
            
        #~ if not self.image_filename:
            #~ self.image_filename = u'-'.join((self.name, self.model_no, '.png')).lower()
            
        super(PartInstance, self).save(force_insert=force_insert, using=using)
        
    # booking should be done from Calendar side
    #~ booking_events = models.ManyToManyField('CalendarEvent', related_name='device_booking_event', blank=True)
    #~ maintenance_events = models.ManyToManyField('CalendarEvent', related_name='device_maintenace_event', blank=True)
    # device state
    # maintainer group
    
    def __str__(self):
        return self.part.name_full or ""
        
    def __repr__(self):
        return self.part.name_full or "" 


class PartOrder(models.Model):
    """ Part Order Process """
    part_order_id =  models.AutoField(primary_key=True)
    namespace = models.ForeignKey(Namespace, related_name='%(app_label)s_%(class)s_namespace_related', 
                      related_query_name="%(app_label)s_%(class)s_namespace",
                      on_delete=models.CASCADE, null=True, blank=True, 
                      help_text="namespace of order ")
    name_full = models.TextField(unique=True, blank=True, 
                            help_text="name of the order")
    order_state = models.ForeignKey(ItemStatus, related_name='%(app_label)s_%(class)s_order_state_related', 
                      related_query_name="%(app_label)s_%(class)s_order_state",
                      on_delete=models.CASCADE, null=True, blank=True, 
                      help_text="state of the order")

    delivery_state = models.ForeignKey(ItemStatus, related_name='%(app_label)s_%(class)s_delivery_state_related', 
                      related_query_name="%(app_label)s_%(class)s_delivery_state",
                      on_delete=models.CASCADE, null=True, blank=True, 
                      help_text="state of the delivery")
    # quote number
    # order number
    # order tracking number
    #~ user_ordered
    #~ user_ordering
    #~ date
    #~ order_state # requested, quote, ordered, arrived
    #~ delivery_date
    #~ shipment_company
    #~ shipment_price
    #~ Tax
    #~ company ordered at ...
    #~ part = models.ForeignKey(Container, related_name="polymer_2order", on_delete=models.CASCADE, blank=True, help_text="container to be ordered")
    #~ user_ordered
    #~ user_ordering
    #~ date
    #~ barcode1D = models.TextField(blank=True, null=True, help_text="1D barcode of location")
    #~ barcode2D = models.BinaryField(blank=True, null=True, help_text="2D barcode of location - binary")
    #~ order_state # requested, quote, ordered, arrived
    
    def __str__(self):
        return self.name_full or ''


#~ class Ordering(models.Model):
    #~ """represents an order
       #~ todo: currency 
    #~ """
    #~ ordering_id =  models.AutoField(primary_key=True)
    #~ #total_price = models.DecimalField(max_digits=8, decimal_places=2) # shall be calculated dynamically, dep. on currency
    #~ shipping = models.DecimalField(max_digits=8, decimal_places=2, null=True)
    #~ tax = models.DecimalField(max_digits=3, decimal_places=2, null=True)
    #~ toll = models.DecimalField(max_digits=3, decimal_places=2, null=True)
    #~ remarks = models.TextField(blank=True)
    #~ description = models.TextField(blank=True)
    #~ user = models.ForeignKey('lara_people.LaraUser', blank=True)

    #~ @property
    #~ def total_cost(self):
        #~ return 42 #self.quantity * self.product.price
    
    #~ class Meta:
        #~ verbose_name_plural = 'orders'
        #~ #unique_together = ("user", "product")
        #~ #ordering = ['-order_id']

    #~ def __str__(self):
        #~ return self.description  or ""
        
    #~ def __repr__(self):
        #~ return u' '.join((self.ordering_id, self.description)) or ""

#~ class ContainerOrder(models.Model):
    #~ """ Container Order Process """
    #~ container_order_id =  models.AutoField(primary_key=True)
    #~ container = models.ForeignKey(Container, related_name="polymer_2order", on_delete=models.CASCADE, blank=True, help_text="container to be ordered")
    #~ user_ordered
    #~ user_ordering
    #~ date
    #~ barcode1D = models.TextField(blank=True, null=True, help_text="1D barcode of location")
    #~ barcode2D = models.BinaryField(blank=True, null=True, help_text="2D barcode of location - binary")
    #~ order_state # requested, quote, ordered, arrived
    # custom save routine
    #~ def save(self, *args, **kwargs): #def save(self, force_insert=None, using=None): #force_insert=force_insert, using=using
        #~ """ """
        #~ # do some custom saves here ...
        #~ super().save(*args, **kwargs)
        
    #~ def __str__(self):
        #~ return self.??? or ""
        
    #~ def __repr__(self):
        #~ return self.??? or ""    

    #~ class Meta:
        #~ verbose_name = 'ForumContribution'
        #~ verbose_name_plural = 'ForumContributions'
        #~ #db_table = "lara_metainfo_item_class"

        
#~ class ProductVendorInfo(models.Model):
    #~ """ Product information provided by vendor 
        #~ todo: currency
    #~ """
    #~ vendorinfo_id =  models.AutoField(primary_key=True)
    #~ vendor = models.ForeignKey('lara_people.Entity', blank=True, null=True)
    #~ product_no = models.TextField(blank=True)
    #~ price = models.DecimalField(max_digits=8, decimal_places=2) # check currency !
    #~ shipping = models.DecimalField(max_digits=8, decimal_places=2)
    #~ tax = models.DecimalField(max_digits=3, decimal_places=2)
    #~ toll = models.DecimalField(max_digits=3, decimal_places=2)
    #~ url = models.TextField(blank=True)
    #~ remarks = models.TextField(blank=True)
    #~ description = models.TextField(blank=True)
    
    #~ def __str__(self):
        #~ return self.description  or ""
        
    #~ def __repr__(self):
        #~ return u' '.join((self.vendorinfo_id, self.description)) or ""
    
    #~ class Meta:
        #~ db_table = "lara_ordering_product_vendor_info"


#~ class Cart(models.Model):
    #~ """ Product information provided by vendor 
        #~ todo: currency
    #~ """
    #~ vendorinfo_id =  models.AutoField(primary_key=True)
    
    #~ def __repr__(self):
        #~ return self.product
